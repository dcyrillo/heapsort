package heapsort;

import java.util.List;

public class Insertionsort {

	public static void insertionSort(List<Integer> vet) {
		for (int j = 1; j < vet.size(); j++) {
			int key = vet.get(j);
			int i = j - 1;
			while ((i > -1) && (vet.get(i) > key)) {
				vet.set(i + 1, vet.get(i));
				i--;
			}
			vet.set(i + 1, key);
		}

	}

}
