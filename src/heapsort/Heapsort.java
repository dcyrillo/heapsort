package heapsort;

import java.util.List;

public class Heapsort {
	public static int parent(int index) {
		return index / 2;
	}

	public static int left(int index) {
		return 2 * index;
	}

	public static int right(int index) {
		return 2 * index + 1;
	}

	public static void swap(List<Integer> vet, int indexA, int indexB, Comparator comparator) {
		int aux;
		aux = vet.get(indexA);
		vet.set(indexA, vet.get(indexB));
		vet.set(indexB, aux);
		comparator.addNumSwaps();
	}

	public static void maxHeapify(List<Integer> vet, int index, int size, Comparator comparator) {
		int auxBigger = index;
		int left = Heapsort.left(index);
		int right = Heapsort.right(index);
		if (left <= size && vet.get(left) > vet.get(index))
			auxBigger = left;
		comparator.addNumComparations();

		if (right <= size && vet.get(right) > vet.get(auxBigger)) {
			auxBigger = right;
			comparator.addNumComparations();
		}

		if (auxBigger != index) {
			Heapsort.swap(vet, index, auxBigger, comparator);
			Heapsort.maxHeapify(vet, index, size, comparator);
			comparator.addNumComparations();
		}

	}

	public static void BuildMaxHeap(List<Integer> vet, int size, Comparator comparator) {

		for (int i = (size-1) / 2; i >= 0; i--) {
			Heapsort.maxHeapify(vet, i, size, comparator);
		}
	}

	public static Comparator Heapsort(List<Integer> vet, int size) {
		
		 
		Comparator comparator = new Comparator();
		Heapsort.BuildMaxHeap(vet, vet.size(), comparator);
		for (int i = vet.size(); i > 0; i--) {
			Heapsort.swap(vet, 1, i, comparator);
			size-=1;
			Heapsort.maxHeapify(vet, 1, vet.size() - 1, comparator);
		}
		return comparator;

	}
}
