package heapsort;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		int testsNumber;
		int vectorSize;

		Random gerador = new Random();
		Scanner read = new Scanner(System.in);

		System.out.println("Quantos casos de teste vocẽ quer ler?");
		testsNumber = read.nextInt();

		for (int i = 0; i < testsNumber; i++) {
			System.out.println("Qual o tamanho do vetor?");
			vectorSize = read.nextInt();
			List<Integer> vet = new ArrayList<Integer>();

			for (int j = 0; j < vectorSize; j++) {
				vet.add(gerador.nextInt(Integer.MAX_VALUE));
			}
			System.out.println("antes" + vet);
			
			// Comparator comparator = Heapsort.Heapsort(vet, vet.size());
			Insertionsort.insertionSort(vet);
			System.out.println("depois" + vet);

			//System.out.println(comparator.toString());

		}

	}

}
