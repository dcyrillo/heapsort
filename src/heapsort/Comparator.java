package heapsort;

public class Comparator {

	private int numSwaps;
	private int numComparations;
	private double processTime;

	public Comparator() {
		this.numComparations = 0;
		this.numSwaps = 0;
		this.processTime = 0;
	}

	public int getNumSwaps() {
		return numSwaps;
	}

	public void setNumSwaps(int numSwaps) {
		this.numSwaps = numSwaps;
	}

	public int getNumComparations() {
		return numComparations;
	}

	public void setNumComparations(int numComparations) {
		this.numComparations = numComparations;
	}

	public double getProcessTime() {
		return processTime;
	}

	public void setProcessTime(double processTime) {
		this.processTime = processTime;
	}

	public void addNumSwaps() {
		this.numSwaps += 1;
	}

	public void addNumComparations() {
		this.numComparations += 1;
	}

	@Override()
	public String toString() {
		return "comparacao: " + this.numComparations + "\n" + "swaps:" + this.numSwaps + "\n" + "processos:"
				+ this.processTime;
	}
}
